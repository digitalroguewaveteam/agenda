@testable import agenda
import Nimble
import XCTest
class NoteTests: XCTestCase {
    
    func testFirstInitNote(){
        
        let note = Note(title: "Title", contenu: NSAttributedString(string: "Note body"), category: 2)
        expect(note.title).to(equal("Title") )
        expect(note.noteBody.string).to(equal("Note body"))
        expect(note.todoAt).to(beNil())
        expect(note.completed).to(beFalse())
        expect(note.priority).to(equal(0))
        expect(note.category).to(equal(2))
        expect(note.id).to(beNil())
        
    }
    
    func testSecondInitNote() {

        let note = Note(title: "Title", noteBody: NSAttributedString(string: "Note body"), category: 2, priority: 1)
        expect(note.title).to(equal("Title") )
        expect(note.noteBody.string).to(equal("Note body"))
        expect(note.todoAt).to(beNil())
        expect(note.completed).to(beFalse())
        expect(note.priority).to(equal(1))
        expect(note.category).to(equal(2))
        expect(note.id).to(beNil())

        }


    func testInitThird(){
        let date = Date()
        let note = Note(id: 1, title: "title", noteBody: NSAttributedString(string: "body"), completed: true, createdAt: date , updatedAt: date, priority: 2, category: 1, todoAt: date)
        
        expect(note.title).to(equal("title") )
        expect(note.noteBody.string).to(equal("body"))
        expect(note.todoAt).to(equal(date))
        expect(note.completed).to(beTrue())
        expect(note.priority).to(equal(2))
        expect(note.category).to(equal(1))
        expect(note.id).to(equal(1))
        expect(note.createdAt).to(equal(date))
        expect(note.updatedAt).to(equal(date))
        expect(note.todoAt).to(equal(date))
        

    }
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        
    }
}
