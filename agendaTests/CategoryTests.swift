//
//  CategoryTests.swift
//  agendaTests
//
//  Created by hamza chamza on 7/24/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import XCTest
import Nimble
@testable import agenda
class CategoryTests: XCTestCase {
    
    func testInit(){
        let category = Category(name: "category1",color : "#ffffff")
        expect(category.name).to(equal("category1"))
        expect(category.color).to(equal("#ffffff"))
        expect(category.id).to(beNil())
    }
    func testSecondInit(){
        let category = Category(id:1,name:"category2", color: "#000000", createdAt:Date(), updatedAt: Date())
        expect(category.name).to(equal("category2"))
        expect(category.color).to(equal("#000000"))
        expect(category.id).to(equal(1))
    }
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
