//
//  BDTest.swift
//  agendaTests
//
//  Created by hamza chamza on 7/25/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import Foundation
import SQLite


class DB {
    static let instance =  DB()
    //MARK:- NoteCulmns
    private var db: Connection?
    private let notes = Table("notes")
    private let id = Expression<Int64>("id")
    private let title = Expression<String>("title")
    private let body = Expression<Data>("noteBody")
    private let completed = Expression<Bool>("completed")
    private let createdAt = Expression<Date>("createdAt")
    private let  updatedAt = Expression<Date>("updateAt")
    private let priority  = Expression<Int>("priority")
    private let category = Expression<Int>("category")
    private let todoAt = Expression<Date?>("todoAt")
    //MARK:- Category Culumns
    private let categories = Table("categories")
    private let categoryId = Expression<Int>("id")
    private let categoryName = Expression<String>("name")
    private let categoryColor = Expression<String>("color")
    private let categoryCreateAt =  Expression<Date>("createdAt")
    private let categoryUpdatedAt = Expression<Date>("updatedAt")
    // Mark:- UserComumns
    private let user = Table("user")
    private let userId = Expression<Int>("id")
    private let userEmail = Expression<String>("email")
    private let UserPassWord = Expression<String>("password")
    let virtualNotes = VirtualTable("virtualNotes")
    private let vrtitle = Expression<String>("title")
    private let vrBody = Expression<String>("body")
    
    init(){
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        print("\(path)")
        do{
            db = try Connection("\(path)/db.sqlite3")
            try db!.execute("PRAGMA foreign_keys = ON;")
            print("connection done")
        }catch{
            db =  nil
            print("error connecting to database  \(error)")
        }
        createCategoryTable()
        createNoteTable()
        createVirtualNoteTable()
        createTableUser()
    }
    //MARK -  creation de table
    func createVirtualNoteTable(){
        let config = FTS5Config()
            .column(vrtitle)
            .column(vrBody)
            .column(id)
        do {
            try db!.run(virtualNotes.create(.FTS5(config), ifNotExists: true))
            
        }catch{
            db =  nil
            print("error creating virtualNoteTable  \(error)")
        }
    }
    func createNoteTable(){
        do{
            try db!.run(notes.create(ifNotExists: true) {
                table in
                table.column(id, primaryKey: .autoincrement)
                table.column(title)
                table.column(body)
                table.column(completed)
                table.column(createdAt)
                table.column(updatedAt)
                table.column(priority)
                table.column(todoAt,defaultValue: nil)
                table.column(category, defaultValue: 1)
                table.foreignKey(category, references: categories, categoryId, update: .cascade , delete: .setDefault)
            })
            
        }catch{
            print("Unable to create table: \(error)")
        }
    }
    
    func createCategoryTable(){
        do{
            try db!.run(categories.create(ifNotExists: true) {
                table in
                table.column(categoryId, primaryKey:  .autoincrement)
                table.column(categoryName)
                table.column(categoryColor)
                table.column(categoryCreateAt)
                table.column(categoryUpdatedAt)
            })
        }catch{
            print("Unable to create table: \(error)")
        }
    }
    
    func createTableUser(){
        do{
            try db!.run(user.create(ifNotExists: true) {
                table in
                table.column(userId,primaryKey: .default)
                table.column(userEmail)
                table.column(UserPassWord)
            })
        }catch{
            print("Unable to create table: \(error)")
        }
    }
}
