import Foundation
import UIKit
extension UIColor {
    
    static  let myBlue =  "4D7CFE" 
    static  let myGreen =  "6DD230"
    static  let myPink = "FE4D97"
    static  let myOrange = "FFAB2B"
    
    func colorFromHex(_ hex : String) -> UIColor {
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if hexString.hasPrefix("#") {
            hexString.remove(at: hexString.startIndex)
        }
        if hexString.count != 6 {
            return UIColor.black
        }
        var rgb : UInt32 = 0
        Scanner(string: hexString).scanHexInt32(&rgb)
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
                            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
                            blue: CGFloat((rgb & 0x0000FF) ) / 255.0,
                            alpha: 1.0)
    }
   
}
