import Foundation
extension Notification.Name {
    static let  categoryNameUpdated = Notification.Name("categoryNameUpdated")
    static let  notificationShowUpdateOrDeleteCategoryPopUp = Notification.Name("notificationShowUpdateOrDeleteCategoryPopUp")
    static let reloadData = Notification.Name("reloadData")
    static let addNoteNotification = Notification.Name("goToAddNoteNotification")
    static let showAddCategoryPopup = Notification.Name("showAddCategoryPopup")
    static let showNoteSettingPopup = Notification.Name("showSettingPopup")
    static let showDateTimePicker = Notification.Name("showDateTimePicker")
    static let reloadCategories =  Notification.Name("reloadCategories")
}
