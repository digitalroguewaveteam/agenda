import Foundation
import UIKit
extension UITextView {
    func resolveTags(){
        let nsText:NSString = self.text as NSString
        let separators = NSCharacterSet(charactersIn: " \n")
        let words = nsText.components(separatedBy: separators as CharacterSet)
        let attrString = NSMutableAttributedString(attributedString: attributedText)
        for word in words {
            if word.hasPrefix("#") && word.count > 1 {
                let ranges = text.nsRanges(of: word)
                for range in ranges {
                    var stringifiedWord:String = word as String
                    stringifiedWord = String(stringifiedWord.dropFirst())
                    attrString.addAttribute(NSAttributedString.Key.link, value: "user:\(stringifiedWord)", range: range)
                }
            }
        }
        textColor = .black
        attributedText = attrString
    }
    func handleList(){
        let mutableString = NSMutableAttributedString(attributedString: attributedText)
        if let range  = selectedTextRange {
            let location  = offset(from: self.beginningOfDocument , to: range.start)
            let length  = offset(from: range.start, to: range.end)
            let rng  = (self.text! as NSString).lineRange(for: NSRange(location: location, length: length))
            if !(self.text! as NSString).substring(with: rng).hasPrefix("   \u{2022} "){
                mutableString.insert(NSAttributedString(string: "   \u{2022} "), at: rng.lowerBound)
            }
            attributedText = mutableString
            if let newPosition = position(from: self.beginningOfDocument, offset: location+1) {
                selectedTextRange = textRange(from: newPosition, to: newPosition)
            }
        }
    }
    func handleTitle(font:UIFont){
        let mutableString = NSMutableAttributedString(attributedString: attributedText)
        if let range  = selectedTextRange {
            let location  = offset(from: self.beginningOfDocument , to: range.start)
            let length  = offset(from: range.start, to: range.end)
            let rng  = (self.text! as NSString).lineRange(for: NSRange(location: location, length: length))
            mutableString.addAttribute(NSAttributedString.Key.font , value: font, range: rng)
            attributedText = mutableString
            if let newPosition = position(from: self.beginningOfDocument, offset: location) {
                selectedTextRange = textRange(from: newPosition, to: newPosition)
            }
        }
    }
}


