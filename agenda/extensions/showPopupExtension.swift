import Foundation
import UIKit
import PopupDialog
import Sheeeeeeeeet
import Down
import UserNotifications
import MaterialComponents.MaterialBottomSheet
import EventKit
extension ShowPopup {
    func showChooseAddCategorieOrAddNotePopup(view:UIViewController){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let addCategory = UIAlertAction(title: "Add new category", style: .default, handler: { _ in
            NotificationCenter.default.post(name: .showAddCategoryPopup, object: nil)
        })
        //addCategory.setValue(UIImage(named: "bookmark"), forKey: "image")
        let addNote = UIAlertAction(title: "Add new note", style: .default, handler: { _ in
            NotificationCenter.default.post(name: .addNoteNotification, object: nil)
        })
        //addNote.setValue(UIImage(named: "outline-radio_button_checked-24px"), forKey: "image")
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:nil)
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(addNote)
        actionSheet.addAction(addCategory)
        view.present(actionSheet, animated: true, completion:nil)
        actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
            return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
            }.first?.isActive = false
    }
    
    func showChooseCategoryPopup(view: UIViewController){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        for category in HomePageModelView().categoryList {
//            actionSheet.addAction(UIAlertAction(title: category.name, style: .default, handler: { (UIAlertAction) in
//                if let homePage =  view as? HomeViewController {
//                    homePage.selectedCategory  = category
//                }
//                NotificationCenter.default.post(name: .addNoteNotification, object: nil)
//            }))
//        }
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        view.present(actionSheet, animated: true, completion:nil)
        actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
            return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
            }.first?.isActive = false
    }
    func showStettingsNotePopup(view:UIViewController, note:Note){
        let itemCell = ActionSheetItemCell.appearance()
        itemCell.titleFont = .systemFont(ofSize: 20)
        itemCell.subtitleColor = .lightGray
        itemCell.titleColor =  UIColor().colorFromHex("297BCC")
        itemCell.separatorInset = .zero
        let delete = ActionSheetItem(title: "Delete", subtitle: nil, value: 1, image: UIImage(named: "delete"), tapBehavior: .dismiss)
        let  moveTo = ActionSheetItem(title: "Move To" , subtitle: nil, value: 2, image: UIImage(named: "move"), tapBehavior: .dismiss)
        let shareNote = ActionSheetItem(title: "Share" , subtitle: nil, value: 3, image: UIImage(named: "share"), tapBehavior: .dismiss)
        let copyAsNote = ActionSheetItem(title: "CopyAs" , subtitle: nil, value: 4, image: UIImage(named: "copy"), tapBehavior: .dismiss)
        let notice  =  ActionSheetItem(title: "Notice" , subtitle: nil, value: 5, image: UIImage(named: "BlueNotification"), tapBehavior: .dismiss)
        let priority = ActionSheetItem(title: "Set Important" , subtitle: nil, value: 7, image: UIImage(named: "alarm-clock"), tapBehavior: .dismiss)
        let button = ActionSheetCancelButton(title: "Cancel")
        let items = [delete,moveTo,shareNote,copyAsNote,notice,priority,button]
        if note.todoAt == nil {
            notice.tapBehavior = .none
        }
        let popup = ActionSheet(items: items) { sheet, item in
            switch item.value  {
            case 1 as Int : do {
                if let id = note.id {
                    let _ = DB.instance.deleteNote(idNote: id)
                }
//                if let homeView = view as? HomeViewController {
//                    homeView.homeModelView.reloadNotesWhenCategorySelected(category:homeView.homeModelView.categoryList[ homeView.homeModelView.getCategoryIndex(id: note.category)])
//                    homeView.noteTableView.reloadData()
//                }
                }
            case 2 as Int : do {
                let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                if let homeView = view as? HomeViewController {
//                    for category in homeView.homeModelView.categoryList {
//                        actionSheet.addAction(UIAlertAction(title: category.name, style: .default, handler: { (UIAlertAction) in
//                            if let id = category.id {
//                                note.category = id
//                                let _ = DB.instance.updateNote(noteId: note.id!, newNote: note)
//                                homeView.homeModelView.reloadNotesWhenCategorySelected(category: category)
//                                homeView.noteTableView.reloadData()
//                                homeView.selectedCategory = category
//                                homeView.categoriesCollectionView.reloadData()
//                            }
//                        }))
//                    }
                    actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    homeView.present(actionSheet, animated: true, completion:nil)
                    actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
                        return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
                        }.first?.isActive = false
                }
                }
            case 3 as Int : do {
                self.shareNote(view: view, note: note)
                }
            case 4 as Int : do {
                self.showCopyAsPopup(view :view,note: note)
                }
            case 5 as Int : do {
                if let date = note.todoAt {
                    if let difference  = Calendar.current.dateComponents([.minute], from:Date(), to: date).minute {
                        if difference > 6 {
                            self.chooseAlarmDatePopup(view: view, noteId: note.id! , date:date, noteTile: note.title)
                            sheet.dismiss()
                        }
                    }
                }
                }
            default:
                sheet.dismiss(animated: true, completion: nil)
            }
        }
        popup.backgroundView?.backgroundColor = UIColor().colorFromHex("0091FF")
        popup.present(in: view, from: view.view)
    }
    func shareNote(view:UIViewController , note:Note) {
        let text = "\(note.title) \n \n \(note.noteBody)"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        view.present(activityViewController, animated: true, completion: nil)
    }
    func showCopyAsPopup(view:UIViewController,note:Note){
        let itemCell = ActionSheetItemCell.appearance()
        itemCell.titleFont = .systemFont(ofSize: 20)
        itemCell.subtitleColor = .lightGray
        itemCell.titleColor = UIColor().colorFromHex("297BCC")
        itemCell.separatorInset = .zero
        let markDown = ActionSheetItem(title: "MarkDown", subtitle: nil, value: 1, image: UIImage(named: "html-coding"), tapBehavior: .dismiss)
        let xml = ActionSheetItem(title: "XML", subtitle: nil, value: 2, image: UIImage(named: "xml"), tapBehavior: .dismiss)
        let html = ActionSheetItem(title: "HTML", subtitle: nil, value: 3, image: UIImage(named: "delete"), tapBehavior: .dismiss)
        let plainText = ActionSheetItem(title: "Plain Text", subtitle: nil, value: 4, image: UIImage(named: "text"), tapBehavior: .dismiss)
        let button = ActionSheetCancelButton(title: "Cancel")
        let items = [markDown,xml,html,plainText,button]
        let popup = ActionSheet(items: items) { sheet, item in
            switch item.value  {
            case 1 as Int : do {
                UIPasteboard.general.string = try? Down(markdownString: "\(note.title) \n \(note.noteBody)  \n \(note.createdAt)").toCommonMark()
                }
            case 2 as Int : do {
                UIPasteboard.general.string = try? Down(markdownString: "\(note.title) \n \(note.noteBody)  \n \(note.createdAt)").toXML()
                }
            case 3 as Int : do {
                UIPasteboard.general.string = try? Down(markdownString: "\(note.title) \n \(note.noteBody)  \n \(note.createdAt)").toHTML()
                }
            case 4 as Int : do {
                UIPasteboard.general.string = "\(note.title) \n \(note.noteBody)  \n \(note.createdAt)"
                }
            default:sheet.dismiss(animated: true, completion: nil)
            }
        }
        popup.backgroundView?.backgroundColor = UIColor().colorFromHex("0091FF")
        popup.present(in: view, from: view.view)
    }
    func showConvertNoteToTaskPopup(view:UIViewController,note:Note){
        let  dateTimePickerVC = DateTimePickerViewController(nibName: "dateTimePickerViewController", bundle: nil)
        if let date = note.todoAt {
            dateTimePickerVC.selectedDate = date
        }
        let popup = PopupDialog(viewController: dateTimePickerVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown ,
                                tapGestureDismissal: true,
                                panGestureDismissal: false
        )
        let cancelButton = CancelButton(title: "CANCEL", height: 60) {
            popup.dismiss()
        }
        
        let addButton = DefaultButton(title: "SET", height: 60) {
            if let home = view as? HomeViewController {
                UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(note.id ?? -1)"])
                let _ =   DB.instance.setDate(idNote: home.homeModelView.notesList[((home.selectedNoteIndexPath?.row)!)].id!, date: dateTimePickerVC.selectedDate)
                home.homeModelView.reloadNotesWhenCategorySelected(category: home.selectedCategory)
                home.noteTableView.reloadData()
            }
        }
        popup.addButtons([cancelButton, addButton])
        if let _ = note.todoAt {
            let noneButton = DefaultButton(title: "none", height: 60) {
                if let home = view as? HomeViewController {
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(note.id ?? -1)"])
                    let _ =   DB.instance.setDate(idNote: home.homeModelView.notesList[((home.selectedNoteIndexPath?.row)!)].id!, date: nil)
                    home.homeModelView.reloadNotesWhenCategorySelected(category: home.selectedCategory)
                    home.noteTableView.reloadData()
                }
            }
            popup.addButtons([cancelButton,noneButton, addButton])
        }
        view.present(popup, animated: true, completion: nil)
    }
    
    
    func chooseAlarmDatePopup(view:UIViewController, noteId:Int64,date:Date, noteTile:String){
        var noticeDate:Date? = nil
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if !granted {
                return
            }
        }
        let popup = ActionSheet(items: initNotificationPopup(noteId:noteId,date:date)) { sheet, item in
            switch item.value  {
            case 0 as Int : do {
                noticeDate = nil
                }
            case 1 as Int : do {
                noticeDate = date
                }
            case 2 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -5*60)
                }
            case 3 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -10*60)
                }
            case 4 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -15*60)
                }
            case 5 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -30*60)
                }
            case 6 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -60*60)
                }
            case 7 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -120*60)
                }
            case 8 as Int : do {
                noticeDate = date.addTime(date: date, addTime: -24*60*60)
                }
            default:do{}
            }
            if item.isOkButton {
                if let dt = noticeDate{
                    self.setNotification(id: noteId, date: dt, body: noteTile)
                }else{
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["\(noteId)"])
                }
            }
        }
        popup.backgroundView?.backgroundColor = UIColor().colorFromHex("0091FF")
        popup.present(in: view, from: view.view)
    }
    
    
    fileprivate func setNotification(id:Int64,date:Date,body:String){
        let content = UNMutableNotificationContent()
        content.title = "You Have a task to do"
        content.body = body
        content.sound = UNNotificationSound.default
        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: date), repeats: false)
        let request = UNNotificationRequest(identifier: "\(id)", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    fileprivate func initNotificationPopup(noteId:Int64,date:Date)->[ActionSheetItem]{
        var items : [ActionSheetItem] = []
        let itemCell = ActionSheetSelectItemCell.appearance()
        itemCell.selectedIcon = UIImage(named: "outline-radio_button_checked-24px-blue")
        itemCell.unselectedIcon = UIImage(named: "circle-outline")
        itemCell.titleFont = .systemFont(ofSize: 20)
        itemCell.subtitleColor = .lightGray
        itemCell.titleColor = UIColor().colorFromHex("297BCC")
        itemCell.separatorInset = .zero
        let none = ActionSheetSingleSelectItem(title: "none", subtitle: nil, isSelected:false, group: "hamza", value: 0, image: nil, tapBehavior: .none)
        let atTheMoment = ActionSheetSingleSelectItem(title: "At the Moment", subtitle: nil, isSelected: false, group: "hamza", value: 1, image: nil, tapBehavior: .none)
        let before5min = ActionSheetSingleSelectItem(title: "before 5 min", subtitle: nil, isSelected: false, group: "hamza", value: 2, image: nil, tapBehavior: .none)
        let before10min = ActionSheetSingleSelectItem(title: "before 10 min", subtitle: nil, isSelected: false, group: "hamza", value: 3, image: nil, tapBehavior: .none)
        let before15min = ActionSheetSingleSelectItem(title: "before 15 min", subtitle: nil, isSelected: false, group: "hamza", value: 4, image: nil, tapBehavior: .none)
        let before30min = ActionSheetSingleSelectItem(title: "before 30 min", subtitle: nil, isSelected: false, group: "hamza", value: 5, image: nil, tapBehavior: .none)
        let beforeOneHour = ActionSheetSingleSelectItem(title: "before 1 H", subtitle: nil, isSelected: false, group: "hamza", value: 6, image: nil, tapBehavior: .none)
        let beforeTwoHour = ActionSheetSingleSelectItem(title: "before 2 H", subtitle: nil, isSelected: false, group: "hamza", value: 7, image: nil, tapBehavior: .none)
        let beforeOneDay = ActionSheetSingleSelectItem(title: "before 1 Day", subtitle: nil, isSelected: false, group: "hamza", value: 8, image: nil, tapBehavior: .none)
        let button = ActionSheetCancelButton(title: "Cancel")
        let set = ActionSheetOkButton(title: "Validate")
        let semaphore = DispatchSemaphore(value: 0)
        UNUserNotificationCenter.current().getPendingNotificationRequests { res in
            var notificationFound:Bool = false
            for request in res {
                if request.identifier == "\(noteId)"{
                    notificationFound = true
                    let trigger = request.trigger as! UNCalendarNotificationTrigger
                    if let triggerDate = trigger.nextTriggerDate() {
                        let difference  = Calendar.current.dateComponents([.minute], from:triggerDate, to: date).minute
                        switch difference {
                        case 0 : atTheMoment.isSelected = true
                        case 5 : before5min.isSelected = true
                        case 10 : before10min.isSelected = true
                        case 15 : before15min.isSelected = true
                        case 30 : before30min.isSelected = true
                        case 60 : beforeOneHour.isSelected = true
                        case 120 : beforeTwoHour.isSelected = true
                        case  24*60: beforeOneDay.isSelected = true
                        default:none.isSelected = true
                        }
                    }
                }
            }
            none.isSelected = (notificationFound == false) ? true : false
            semaphore.signal()
        }
        semaphore.wait()
        if let difference  = Calendar.current.dateComponents([.minute], from:Date(), to: date).minute {
            if difference < 12 {
                items = [atTheMoment,before5min,before10min,none,set,button]
            } else if difference < 17 {
                items = [atTheMoment,before5min,before10min,before15min,none,set,button]
            }else if difference < 32  {
                items = [atTheMoment,before5min,before10min,before15min,before30min,none,set,button]
            }else if difference < 62  {
                items = [atTheMoment,before5min,before10min,before15min,before30min,beforeOneHour,none,set,button]
            }else if difference < 122  {
                items = [atTheMoment,before5min,before10min,before15min,before30min,beforeOneHour,none,set,button]
            }else{
                items = [atTheMoment,before5min,before10min,before15min,before30min,beforeOneHour,beforeTwoHour,beforeOneDay,none,set,button]
            }
        }
        return items
    }
    
    func showSignInPopUp(view:UITableViewController){
        let vc = PopupSignInViewController(nibName: "PopupSignInViewController", bundle: nil)
        let bottomSheet: MDCBottomSheetController = MDCBottomSheetController(contentViewController: vc)

        view.present(bottomSheet, animated: true, completion: nil)
}
}
