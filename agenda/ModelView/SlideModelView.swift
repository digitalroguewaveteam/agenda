//
//  SlideModelView.swift
//  agenda
//
//  Created by hamza chamza on 8/20/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import Foundation
import UIKit
class SlideModelView {
    var categories : [Category]
    init(){
       categories = DB.instance.getCategories()
    }
    func loadCategories(){
        categories = DB.instance.getCategories()
    }
    func asignDataToNoteTableView(cell:CategoryTableViewCell, indexPath:IndexPath){
        cell.categoryName.text =  categories[indexPath.row].name
        switch categories[indexPath.row].color{
        case UIColor.myBlue: cell.categoryColor.image =  UIImage(named: "blue-circle")
        case UIColor.myPink: cell.categoryColor.image =  UIImage(named: "pink-circle")
        case UIColor.myGreen: cell.categoryColor.image =  UIImage(named: "green-circle")
        default:
            cell.categoryColor.image =  UIImage(named: "orange-circle")
        }
    }
    func getCategoryIndex(category:Category)->Int?{
        for i in 0...categories.count-1 {
            if categories[i].id == category.id {
                return i
            }
        }
        return nil
    }
}
