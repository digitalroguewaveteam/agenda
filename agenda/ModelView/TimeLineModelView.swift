import Foundation
import UIKit
class TimeLineModelView {
    var taskList = [Note]()
    init (date:Date){
        taskList = DB.instance.getNotesBytodoAtdate(date: date)
    }
    func loadTasksDate(date:Date){
         taskList = DB.instance.getNotesBytodoAtdate(date: date)
    }
    func taskTableViewNumberOfRowsInSection(tableView:UITableView)-> Int{
        if taskList.count == 0  {
            let noDataFrame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height)
            let noData: UILabel = UILabel(frame: noDataFrame)
            noData.text = "No Tasks!"
            noData.textColor = .black
            noData.textAlignment = .center
            tableView.backgroundView = noData
            tableView.separatorStyle = .none
        }else{
            tableView.backgroundView = .none
        }
        return  taskList.count
    }
    func assignTaskTableData(cell:TaskTableViewCell,indexPath:IndexPath){
        cell.title.text = taskList[indexPath.row].title
        guard let todoAt = taskList[indexPath.row].todoAt else {return}
        cell.todoTime.text = Date().todoAtHour(date: todoAt)
        
            if !taskList[indexPath.row].completed {
                cell.doneImage.isHidden = true
            }
       
        cell.categoryColorView.backgroundColor = UIColor().colorFromHex(DB.instance.getCategoryColor(id: taskList[indexPath.row].category))
        
    }
    func longTapHandler(cell:TaskTableViewCell,indexPath:IndexPath){
        if taskList[indexPath.row].completed {
            DB.instance.taskUncompleted(idNote: taskList[indexPath.row].id!)
            cell.doneImage.isHidden = true
            taskList[indexPath.row].completed = false
        }else{
            DB.instance.taskCompleted(idNote: taskList[indexPath.row].id!)
            taskList[indexPath.row].completed = true
            cell.doneImage.isHidden = false
        }
        cell.layoutIfNeeded()
    }
    func getCategoryById(id:Int)->Category{
      return  DB.instance.getCategoryById(id: id)
    }
}
