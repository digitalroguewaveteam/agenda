import Foundation
import UIKit
class HomePageModelView {
    let reloadDataNotification = NotificationCenter.default
    var notesList = [Note]()
    var fiteredNotes : [Note]?
    init(){
        notesList = DB.instance.getAllNotes()
        reloadDataNotification.addObserver(self, selector: #selector(loadData), name: Notification.Name("reloadData"), object: nil)
    }
    @objc func loadData(){
        loadNotes()
    }
    func searchFullText(text:String){
     fiteredNotes =  DB.instance.getFullTextearchResult(searchedText: text)
    }
    func loadNotes(){
        notesList = DB.instance.getAllNotes()
    }
   
    func addNewEmptyNote(selectedCategory:Category){
        
        _ = DB.instance.addNote(note: Note(title: "Untitled"  , contenu: NSAttributedString(string: "Empty Note",attributes: [NSAttributedString.Key.font : UIFont(name: "SFProText-Regular", size: 14) as Any ]), category: selectedCategory.id!))
            notesList = DB.instance.getNotesCategory(selectedCategory: selectedCategory)
    }
//    func findCategoryById(id : Int) ->  Category? {
//        let categoryResult :Category?
//        for category in categoryList {
//            if category.id == id{
//                categoryResult = category
//                return categoryResult!
//            }
//        }
//        return nil
//    }
    func assignDataWhileSearching(cell:NoteTableViewCell, indexPath:IndexPath){
        guard let list = fiteredNotes else {return}
        cell.titreNote.text = list[indexPath.row].title
        cell.noteTextView.resolveTags()
//        cell.categoryName.text = findCategoryById(id: notesList[indexPath.row].category)?.name
//        cell.categoryName.textColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
//        cell.topColorBar.backgroundColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
//        cell.colorBar.backgroundColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
//        cell.categoryName.text = findCategoryById(id: notesList[indexPath.row].category)?.name
//        cell.categoryName.textColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
        cell.noteTextView.attributedText =   notesList[indexPath.row].noteBody
        if let todoAt = list[indexPath.row].todoAt {
            cell.dateLabel.text = Date().todoAtDateFormatter(date: todoAt)
        }else {
            cell.dateLabel.text = ""
        }
        cell.lastModificationLabel.text = "Added: \(Date().createdAtDateFormatter(date: list[indexPath.row].createdAt ))"
        if cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font: cell.noteTextView.font!) < cell.noteTextViewHeightContstraint.constant - cell.noteTextView.font!.lineHeight {
            cell.noteTextView.sizeThatFits(cell.frame.size)
            cell.noteTextViewHeightContstraint.constant = cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font:cell.noteTextView.font!) +
                2*cell.noteTextView.font!.lineHeight
            cell.layoutIfNeeded()
        }
    }
    func reloadNotesWhenCategorySelected(category:Category?){
        if let notNilCategory = category {
            notesList = DB.instance.getNotesCategory(selectedCategory: notNilCategory)
        }else{
            notesList = DB.instance.getAllNotes()
        }
    }
    func updateNote(index:Int,noteBody:NSAttributedString?,title:NSAttributedString?){
         let note = notesList[index]
      
            if let titre = title {
                note.title =  titre.string
            }
            if let text = noteBody {
                note.noteBody =  text
            }
            guard  let id = notesList[index].id else {
                return
            }
            let _ = DB.instance.updateNote(noteId: id, newNote: note)
    }
    func asignDataToNoteTableView(cell:NoteTableViewCell, indexPath:IndexPath){
         cell.noteTextView.allowsEditingTextAttributes = true
        cell.titreNote.text = notesList[indexPath.row].title
        cell.noteTextView.resolveTags()
//        cell.categoryName.text = findCategoryById(id: notesList[indexPath.row].category)?.name
//        cell.categoryName.textColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
//        cell.topColorBar.backgroundColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
//        cell.colorBar.backgroundColor = UIColor().colorFromHex(findCategoryById(id: notesList[indexPath.row].category)?.color ?? "FFFFFF")
        cell.titreNote.text = notesList[indexPath.row].title
            cell.noteTextView.attributedText =    notesList[indexPath.row].noteBody
        
        if let todoAt = notesList[indexPath.row].todoAt {
            cell.dateLabel.text = Date().todoAtDateFormatter(date: todoAt)
        }else {
            cell.dateLabel.text = ""
        }
        cell.calendarIcon.isHidden = true
        cell.settingsIcon.isHidden = true
        cell.lastModificationLabel.text = "Added: \(Date().createdAtDateFormatter(date: notesList[indexPath.row].createdAt ))"
        cell.lastModificationLabel.isHidden = true
}
    func noteTableViewNumberOfRowsInSection(tableView:UITableView)-> Int{
        if notesList.count == 0 {
            let noDataFrame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height)
            let noData: UILabel = UILabel(frame: noDataFrame)
            noData.text = "No Items!"
            noData.textColor = .black
            noData.textAlignment = .center
            tableView.backgroundView = noData
            tableView.separatorStyle = .none
        }else{
            tableView.backgroundView = .none
        }
        return  notesList.count
    }

    func getCategoryById(id:Int)->Category{
        return  DB.instance.getCategoryById(id: id)
    }
}
