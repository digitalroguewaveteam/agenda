//
//  CategoryTableViewCell.swift
//  agenda
//
//  Created by hamza chamza on 8/20/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var categoryColor: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
