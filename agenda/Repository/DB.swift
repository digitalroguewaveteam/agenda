//
//  DB.swift
//  agenda
//
//  Created by hamza chamza on 8/6/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//
import SQLite
import Foundation
class DB {
    static let instance =  DB()
    //MARK:- NoteCulmns
    private var db: Connection?
    private let notes = Table("notes")
    private let id = Expression<Int64>("id")
    private let title = Expression<String>("title")
    private let body = Expression<Data>("noteBody")
    private let completed = Expression<Bool>("completed")
    private let createdAt = Expression<Date>("createdAt")
    private let  updatedAt = Expression<Date>("updateAt")
    private let priority  = Expression<Int>("priority")
    private let category = Expression<Int>("category")
    private let todoAt = Expression<Date?>("todoAt")
    //MARK:- Category Culumns
    private let categories = Table("categories")
    private let categoryId = Expression<Int>("id")
    private let categoryName = Expression<String>("name")
    private let categoryColor = Expression<String>("color")
    private let categoryCreateAt =  Expression<Date>("createdAt")
    private let categoryUpdatedAt = Expression<Date>("updatedAt")
    // Mark:- UserComumns
    private let user = Table("user")
    private let userId = Expression<Int>("id")
    private let userEmail = Expression<String>("email")
    private let UserPassWord = Expression<String>("password")
    let virtualNotes = VirtualTable("virtualNotes")
    private let vrtitle = Expression<String>("title")
    private let vrBody = Expression<String>("body")
    
    init(){
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        print("\(path)")
        do{
            db = try Connection("\(path)/db.sqlite3")
            try db!.execute("PRAGMA foreign_keys = ON;")
            print("connection done")
        }catch{
            db =  nil
            print("error connecting to database  \(error)")
        }
        createCategoryTable()
        createNoteTable()
        createVirtualNoteTable()
        createTableUser()
    }
    //MARK -  creation de table
    func createVirtualNoteTable(){
        let config = FTS5Config()
            .column(vrtitle)
            .column(vrBody)
            .column(id)
        do {
            try db!.run(virtualNotes.create(.FTS5(config), ifNotExists: true))
            
        }catch{
            db =  nil
            print("error creating virtualNoteTable  \(error)")
        }
    }
    func createNoteTable(){
        do{
            try db!.run(notes.create(ifNotExists: true) {
                table in
                table.column(id, primaryKey: .autoincrement)
                table.column(title)
                table.column(body)
                table.column(completed)
                table.column(createdAt)
                table.column(updatedAt)
                table.column(priority)
                table.column(todoAt,defaultValue: nil)
                table.column(category, defaultValue: 1)
                table.foreignKey(category, references: categories, categoryId, update: .cascade , delete: .setDefault)
            })
            
        }catch{
            print("Unable to create table: \(error)")
        }
    }
    func createCategoryTable(){
        do{
            try db!.run(categories.create(ifNotExists: true) {
                table in
                table.column(categoryId, primaryKey:  .autoincrement)
                table.column(categoryName)
                table.column(categoryColor)
                table.column(categoryCreateAt)
                table.column(categoryUpdatedAt)
            })
        }catch{
            print("Unable to create table: \(error)")
        }
    }
    func createTableUser(){
        do{
            try db!.run(user.create(ifNotExists: true) {
                table in
                table.column(userId,primaryKey: .default)
                table.column(userEmail)
                table.column(UserPassWord)
            })
        }catch{
            print("Unable to create table: \(error)")
        }
    }
    func getFullTextearchResult(searchedText:String)->[Note]?{
        var notes = [Note]()
        do {
            let replies = try db!.prepare(virtualNotes.filter(vrBody.match(searchedText) || vrtitle.match(searchedText)))
            for note in replies {
                if let nt = getNoteById(noteId: note[id]){
                    notes.append(nt)
                }
            }
        }catch{
            
        }
        if notes.isEmpty {
            return nil
        }
        return notes
    }
    
    func saveUser(_ u:User){
        do {
            try db!.run(user.insert(userId <- u.id,userEmail <- u.email,UserPassWord <- u.password))
        }catch{
            print("insert Failed \(error)")
        }
    }
    func getUser()->User?{
        var user:User?
        do {
            for row in try db!.prepare(self.user) {
                user = User(id: row[userId], password: row[UserPassWord], email: row[userEmail])
            }
        }catch{
            print("select failed \(error)")
        }
        return user
    }
    func deleteUser(){
        do {
            try db!.run(user.select(user[*]).delete())
        }catch{
            print("delete failed : \(error)")
        }
    }
    func updateUser(newUser:User){
        do {
            try db!.run(user.filter(userId == newUser.id).update([
                userId <- newUser.id,
                UserPassWord <- newUser.password,
                userEmail <- newUser.email
                ]))
        }catch{
            print("update failed : \(error)")
        }
    }
    func addNote(note: Note) -> Int64{
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: note.noteBody, requiringSecureCoding: false)
            
            let insert = notes.insert(title <- note.title ,body <- data, completed <- note.completed, createdAt <- note.createdAt , updatedAt <- note.updatedAt, priority <- note.priority,  category <- note.category)
            let idRow = try db!.run(insert) // return Int64
            try db!.run(virtualNotes.insert(id <- idRow,vrtitle <- note.title,vrBody <- note.noteBody.string))
            return idRow
        }catch{
            print("insert failed \(error)")
            return -1
        }
        
    }
    func deleteNote(idNote :Int64) -> Bool{
        do{
            try db!.run(notes.filter(id == idNote).delete())
            try db!.run(virtualNotes.filter(id == idNote).delete())
            return true
        }catch{
            print("Delete failed \(error) ")
            return false
        }
    }
    func updateNote(noteId :Int64 , newNote:Note) -> Bool {
        let note = notes.filter(id == noteId)
        do{
            let data = try NSKeyedArchiver.archivedData(withRootObject: newNote.noteBody, requiringSecureCoding: false)
            let update = note.update([
                title <- newNote.title,
                body <- data ,
                completed <- newNote.completed,
                createdAt <- newNote.createdAt ,
                updatedAt <- newNote.updatedAt,
                priority <- newNote.priority,
                category <- newNote.category
                ])
            
            if try db!.run(update) > 0 {
                if updateVrNote(idNote: noteId, body: newNote.noteBody.string, title: newNote.title) == true{
                    return true
                }
            }
            
        }catch{
            print("Update failed: \(error)")
        }
        return false
    }
    func updateVrNote(idNote:Int64,body:String,title:String)->Bool{
        let note = virtualNotes.filter(id == idNote)
        do{
            let update = note.update([
                vrtitle <- title,
                vrBody <- body
                ])
            
            if try db!.run(update) > 0 {
                return true
            }
        }catch{
            print("Update failed: \(error)")
        }
        return false
    }
    
    func getAllNotes() -> [Note] {
        var notes = [Note]()
        do {
            for note in try db!.prepare(self.notes.order(createdAt.desc)) {
                
                notes.append(Note(id : note[id],
                                  title: note[title],
                                  noteBody :  try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(note[body]) as! NSAttributedString ,
                                  completed : note[completed],
                                  createdAt : note[createdAt],
                                  updatedAt : note[updatedAt],
                                  priority: note[priority],
                                  category: note[category],
                                  todoAt: note[todoAt]
                ))
            }
        }catch{
            print("select failed \(error)")
        }
        return notes
    }
    
    //MARK:- CRUD category
    func addCategory(category: Category) -> Int64{
        do {
            let insert = categories.insert(categoryName <- category.name, categoryColor <- category.color, categoryCreateAt <- category.createdAt, categoryUpdatedAt <- category.updatedAt)
            let idRow = try db!.run(insert) // return Int64
            return idRow
        }catch{
            print("insert category failed \(error)")
            return -1
        }
    }
    func deleteCategory(id: Int) -> Bool{
        do{
            let category = categories.filter(categoryId == id)
            try db!.run(category.delete())
            return true
        }catch{
            print("Delete category failed \(error) ")
            return false
        }
    }
    func getCategories() ->[Category] {
        var categories = [Category]()
        do {
            for category in try db!.prepare(self.categories.order(categoryName)) {
                categories.append(Category(id :category[categoryId],name: category[categoryName], color: category[categoryColor] ,createdAt: category[categoryCreateAt], updatedAt: category[categoryUpdatedAt]  ))
            }
        }catch{
            print("select category failed \(error)")
        }
        return categories
    }
    func updateCategory(id:Int , newCategory:Category) -> Bool {
        let category = categories.filter(categoryId == id)
        do{
            let update = category.update([
                categoryName <- newCategory.name,
                categoryColor <- newCategory.color,
                categoryCreateAt <- newCategory.createdAt,
                categoryUpdatedAt <- newCategory.updatedAt
                ])
            if try db!.run(update) > 0 {
                return true
            }
        }catch{
            print("Update category failed: \(error)")
        }
        return false
    }
    //MARK:- Close database
    func closeDatabase(){
        db =  nil
    }
    //MARK:- getSortedNotes
    func getNotesSortedByDateLastModified() -> [Note]{
        var notes = [Note]()
        do {
            for note in try db!.prepare(self.notes.order(priority,updatedAt)) {
                notes.append(Note(id : note[id],
                                  title: note[title],
                                  noteBody : try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(note[body]) as! NSAttributedString,
                                  completed : note[completed],
                                  createdAt : note[createdAt],
                                  updatedAt : note[updatedAt],
                                  priority: note[priority],
                                  category: note[category],
                                  todoAt: note[todoAt]
                ))
            }
        }catch{
            print("select failed \(error)")
        }
        return notes
    }
    
    func  getNotesCategory(selectedCategory: Category) -> [Note]{
        var tasks = [Note]()
        if let idSelectedCategory = selectedCategory.id {
            do {
                for note in try db!.prepare(self.notes.order(createdAt.desc).filter(category == idSelectedCategory)) {
                    tasks.append(Note(id : note[id],
                                      title: note[title],
                                      noteBody : try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(note[body]) as! NSAttributedString,
                                      completed : note[completed],
                                      createdAt : note[createdAt],
                                      updatedAt : note[updatedAt],
                                      priority: note[priority],
                                      category: note[category],
                                      todoAt: note[todoAt]
                    ))
                }
            }catch{
                print("select failed \(error)")
            }
        }
        return tasks
    }
    
    func resetDB(){
        do{
            try db!.run(notes.delete())
            try db!.run(categories.delete())
        }catch{
            print("\(error)")
        }
    }
    func getNotesBytodoAtdate(date:Date)->[Note]{
        
        var tasksId = [Int64]()
        var tasks = [Note]()
        do {
            for note in try db!.prepare(self.notes.filter(date.startOfDay...date.endOfDay ~= todoAt)) {
                tasksId.append(note[id])
            }
            if !tasksId.isEmpty {
                for idTask in  tasksId.uniqued() {
                    for note in try db!.prepare(self.notes.filter(id == idTask)) {
                        tasks.append(Note(id : note[id],
                                          title: note[title],
                                          noteBody : try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(note[body]) as! NSAttributedString,
                                          completed : note[completed],
                                          createdAt : note[createdAt],
                                          updatedAt : note[updatedAt],
                                          priority: note[priority],
                                          category: note[category],
                                          todoAt: note[todoAt]
                        ))
                    }
                }
            }
        }catch{
            print("select failed \(error)")
        }
        return tasks
    }
    func setDate(idNote:Int64,date:Date?)->Bool{
        let note = notes.filter(id == idNote)
        do{
            let update = note.update([
                todoAt <- date
                ])
            if try db!.run(update) > 0 {
                return true
            }
        }catch{
            print("Update failed: \(error)")
        }
        return false
    }
    func taskCompleted(idNote:Int64){
        let note = notes.filter(id == idNote)
        do{
            let update = note.update([
                completed <- true
                ])
            try db!.run(update)
        }catch{
            print("Update failed: \(error)")
        }
    }
    func taskUncompleted(idNote:Int64){
        let note = notes.filter(id == idNote)
        do{
            let update = note.update([
                completed <- false
                ])
            try db!.run(update)
        }catch{
            print("Update failed: \(error)")
        }
    }
    func getCategoryColor(id:Int)->String{
        var color:String!
        do {
            for category in try (db?.prepare(self.categories.filter(id == categoryId)))!{
                color = category[categoryColor]
            }
        }catch{
            print("\(error)")
        }
        return color
    }
    func getCategoryById(id:Int)-> Category{
        var cat:Category!
        do {
            for category in try db!.prepare(self.categories.filter(categoryId == id)) {
                cat = Category(id :category[categoryId],name: category[categoryName], color: category[categoryColor] ,createdAt: category[categoryCreateAt], updatedAt: category[categoryUpdatedAt])
            }
        }catch{
            print("\(error)")
        }
        return cat
    }
    func getNoteById(noteId:Int64)->Note?{
        var nt:Note?
        do {
            for note in try db!.prepare(self.notes.filter(id == noteId)){
                nt = Note(    id : note[id],
                              title: note[title],
                              noteBody : try! NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(note[body]) as! NSAttributedString,
                              completed : note[completed],
                              createdAt : note[createdAt],
                              updatedAt : note[updatedAt],
                              priority: note[priority],
                              category: note[category],
                              todoAt: note[todoAt]
                )
            }
        }catch{
            print("\(error)")
        }
        return nt
    }
    func getLastAddedCategory()->Category{
        let categories = getCategories()
        var lastCategory :Category = categories[0]
        for category in categories {
            if category.createdAt > lastCategory.createdAt {
                lastCategory = category
            }
        }
        return lastCategory
    }
}
