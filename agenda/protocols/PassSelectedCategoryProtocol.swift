//
//  passSelectedCategoryProtocol.swift
//  agenda
//
//  Created by hamza chamza on 8/20/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import Foundation
protocol PassSelectedCategoryProtocol {
    func receivedCategory(category:Category)
}
