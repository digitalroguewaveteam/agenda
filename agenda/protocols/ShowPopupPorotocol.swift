import Foundation
import UIKit
protocol  ShowPopup {
    func showChooseAddCategorieOrAddNotePopup(view:UIViewController)
    func showChooseCategoryPopup(view: UIViewController)
}
