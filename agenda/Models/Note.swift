import Foundation
class Note : Executable {
    var todoAt: Date?
    var id: Int64?
    var title: String
    var noteBody: NSAttributedString
    var completed : Bool
    var createdAt  : Date
    var updatedAt: Date
    var priority : Int
    var category: Int
    
    init (title: String, contenu: NSAttributedString ,category : Int){
        self.title = title
        self.noteBody =  contenu
        self.category = category
        self.createdAt  = Date()
        self.updatedAt = Date()
        self.priority = 0 //  default 0
        self.completed = false
    }
    init (title: String, noteBody: NSAttributedString ,category: Int, priority: Int){
        self.title = title
        self.noteBody =  noteBody
        self.category = category
        self.createdAt  = Date()
        self.updatedAt = Date()
        self.priority = priority
        self.completed = false
    }
    init(id: Int64,title: String, noteBody: NSAttributedString, completed: Bool, createdAt: Date, updatedAt: Date, priority: Int, category: Int,todoAt:Date?){
        self.title = title
        self.noteBody =  noteBody
        self.category = category
        self.createdAt  = createdAt
        self.updatedAt = updatedAt
        self.priority = priority
        self.completed = completed
        self.id = id
        self.todoAt = todoAt
    }
}
