import Foundation
class Category{
    var id: Int?
    var name: String
    var color: String
    var createdAt: Date
    var updatedAt: Date
    init (name:String, color:String){
        self.name = name
        self.color = color
        self.createdAt = Date()
        self.updatedAt = Date()
    }
    init (id: Int ,  name:String, color:String, createdAt:Date, updatedAt:Date){
        self.name = name
        self.color = color
        self.id = id
        self.createdAt = createdAt
        self.updatedAt = updatedAt
    }
}
