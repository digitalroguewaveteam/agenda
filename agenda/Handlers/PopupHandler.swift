//
//  PopupHandler.swift
//  agenda
//
//  Created by hamza chamza on 8/21/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
import UserNotifications
import EventKit
class PopupHandler {
   static func showAddCategoryPopup()->PopupDialog{
        let addCategoryVC = PopUpAddCategoryViewController(text: nil , color: nil)
        let popup = PopupDialog(viewController: addCategoryVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: false
        )
        let cancelButton = CancelButton(title: "CANCEL", height: 60) {
            popup.dismiss()
        }
        let addButton = DefaultButton(title: "ADD", height: 60) {
            let categoryName = (addCategoryVC.categoryName.isEmpty) ? "Untitled" : addCategoryVC.categoryName
            let categoryColor: String = addCategoryVC.categoryColor ?? UIColor.myBlue
            let toAddCategory = Category(name: categoryName, color: categoryColor)
            let _ =    DB.instance.addCategory(category: toAddCategory) // toAddCategory.id == nil
            let addedCategory  = DB.instance.getLastAddedCategory()
            NotificationCenter.default.post(name: .reloadCategories, object: nil, userInfo: ["newCategory":addedCategory])
        }
        popup.addButtons([cancelButton, addButton])
        return popup
    }
    static func showUpdateOrDeleteCategoryPopup(idCategory:Int) ->PopupDialog{
        let category = DB.instance.getCategoryById(id: idCategory)
        let addCategoryVC = PopUpAddCategoryViewController(text: category.name , color: category.color)
        let popup = PopupDialog(viewController: addCategoryVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .fadeIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false
        )
        let cancelButton = CancelButton(title: "CANCEL", height: 60) {
            popup.dismiss()
        }
        let DeleteButton = DefaultButton(title: "DELETE", height: 60) {
            let result = DB.instance.deleteCategory(id: idCategory)
            if result {
                NotificationCenter.default.post(name: .reloadCategories, object: nil)
            }
        }
        let modifyButton = DefaultButton(title: "MODIFY", height: 60) {
            let categoryName = (addCategoryVC.categoryName.isEmpty) ? "Untitled" : addCategoryVC.categoryName
            let categoryColor: String = addCategoryVC.categoryColor ?? UIColor.myBlue
            let modifiedCategory = Category(name: categoryName, color: categoryColor)
            let _ =    DB.instance.updateCategory(id: idCategory, newCategory: modifiedCategory)
            NotificationCenter.default.post(name: .reloadCategories, object: nil)
        }
        popup.addButtons([cancelButton, modifyButton, DeleteButton])
        return popup
    }
}
