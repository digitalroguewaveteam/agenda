//
//  SlideViewController.swift
//  agenda
//
//  Created by hamza chamza on 8/18/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//
import UIKit
import SideMenu
class SlideViewController: UIViewController {
    var passCategoryProtocolDelegate : PassSelectedCategoryProtocol?
    var selectedCategory : Category?
    @IBOutlet weak var categoriesTableView: UITableView!
    var slideModelView =  SlideModelView()
    override func viewDidLoad() {
        definesPresentationContext = true
        super.viewDidLoad()
        categoriesTableView.dataSource = self
        categoriesTableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadCategories(_:)), name: .reloadCategories, object: nil)
    }
    @IBAction func addCategoryButtonTapped(_ sender: UIButton) {
        self.present(PopupHandler.showAddCategoryPopup(), animated: true, completion: nil)
    }
    @objc func reloadCategories(_ notification: NSNotification){
        if let newCategory = notification.userInfo?["newCategory"] as? Category {
            selectedCategory = newCategory
            passCategoryProtocolDelegate?.receivedCategory(category: newCategory)
        }
        slideModelView.loadCategories()
        categoriesTableView.reloadData()
    }
    func selectedCategoryCellLayout(cell:CategoryTableViewCell) {
        cell.contentView.backgroundColor = UIColor().colorFromHex("F8FAFB")
        cell.categoryName.textColor = UIColor().colorFromHex(UIColor.myBlue)
    }
    @objc func updateCategory( _ sender: CategoryTableViewCellLongTapHandler){
        if let id = sender.idCategory {
            if presentedViewController == nil {
                self.present(PopupHandler.showUpdateOrDeleteCategoryPopup(idCategory: id), animated: true, completion: nil)
            }
        }
    }
}
extension SlideViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return slideModelView.categories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  Bundle.main.loadNibNamed("CategoryTableViewCell", owner: self, options: nil)?.first as! CategoryTableViewCell
        slideModelView.asignDataToNoteTableView(cell: cell, indexPath: indexPath)
        if let id  = selectedCategory?.id {
            if  slideModelView.categories[indexPath.row].id == id   {
                selectedCategoryCellLayout(cell: cell)
            }
        }
        let longTapGesture = CategoryTableViewCellLongTapHandler(target: self, action: #selector(updateCategory(_:)))
        longTapGesture.idCategory = slideModelView.categories[indexPath.row].id
        cell.addGestureRecognizer(longTapGesture)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            selectedCategoryCellLayout(cell: cell as! CategoryTableViewCell)
        }
        selectedCategory = slideModelView.categories[indexPath.row]
        passCategoryProtocolDelegate?.receivedCategory(category: slideModelView.categories[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CategoryTableViewCell {
            cell.contentView.backgroundColor = UIColor().colorFromHex("FFFFFF")
            cell.categoryName.textColor = UIColor.black
        }
    }
}
