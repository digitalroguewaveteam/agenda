//
//  CategoryTableViewCell.swift
//  agenda
//
//  Created by admin on 01/04/2019.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import UIKit
import SwipeCellKit
class CellTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var bookMarkIcon: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
