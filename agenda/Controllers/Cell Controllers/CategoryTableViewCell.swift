import UIKit
import SwipeCellKit
class CategoryTableViewCell: SwipeTableViewCell {
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var categoryIconImageView: UIImageView!
    var cellId : Int?
    let notificationCategoryNameUpdated = NotificationCenter.default
    var longTapGesture :  UILongPressGestureRecognizer?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryNameTextField.delegate = self
        categoryNameTextField.isEnabled = false
        longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(startEditingTextView))
        if let  longtap = longTapGesture {
            tapView.addGestureRecognizer(longtap)
        }
    }
    @IBAction func didEndEditinCategoryName(_ sender: UITextField) {
        CategoryModelView().updateCategoryName(id: cellId!, NewCategoryName: sender.text ?? categoryNameTextField.text!)
        notificationCategoryNameUpdated.post(name: .categoryNameUpdated, object: nil)
    }
    @objc func startEditingTextView(){
        categoryNameTextField.isEnabled = true
        categoryNameTextField.borderStyle = .roundedRect
        resignFirstResponder()
        categoryNameTextField.becomeFirstResponder()
    }
    func endEditing(){
        tapView.endEditing(true)
        categoryNameTextField.isEnabled = false
        categoryNameTextField.borderStyle = .none
    }
}
extension CategoryTableViewCell : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}

