import UIKit
class NoteTableViewCell: UITableViewCell {
    @IBOutlet weak var titreNote: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var calendarIcon: UIButton!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var topColorBar: UIView!
    @IBOutlet weak var colorBar: UIView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var lastModificationLabel: UILabel!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var settingsIcon: UIButton!
    @IBOutlet weak var noteTextViewHeightContstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func settingButtonTouched(_ sender: UIButton) {
         NotificationCenter.default.post(name: .showNoteSettingPopup, object: nil)
    }
    @IBAction func calendarButtonTouched(_ sender: UIButton) {
        NotificationCenter.default.post(name: .showDateTimePicker , object: nil)
    }
}

