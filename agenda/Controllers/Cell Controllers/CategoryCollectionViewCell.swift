import UIKit
class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var categoryNameEditText: UITextField!
    let notificationShowUpdateOrDeleteCategoryPopUp = NotificationCenter.default
    var categoryId : [String: Int]?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        self.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(updateOrDeleteCategory)))
        }
    @objc func updateOrDeleteCategory(){
        if let id = categoryId {
             notificationShowUpdateOrDeleteCategoryPopUp.post(name: .notificationShowUpdateOrDeleteCategoryPopUp, object: nil, userInfo: id)
        }
    }
}
