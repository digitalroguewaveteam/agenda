import UIKit

class TaskTableViewCell: UITableViewCell ,ShowPopup{
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var todoTime: UILabel!
   
    @IBOutlet weak var doneImage: UIImageView!
    
    
    @IBOutlet weak var categoryColorView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 3, left: 3, bottom: 2, right: 2))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
