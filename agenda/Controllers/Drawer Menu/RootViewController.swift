//
//  RootViewController.swift
//  agenda
//
//  Created by hamza chamza on 8/5/19.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import Foundation
import UIKit
protocol RootViewControllerDelegate: class {
    func rootViewControllerDidTapMenuButton(_ rootViewController: RootViewController)
}
class RootViewController: UIViewController, UINavigationControllerDelegate  {
    <#code#>
}
