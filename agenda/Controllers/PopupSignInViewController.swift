import UIKit
import GoogleSignIn

import FirebaseAuth
import FirebaseUI
class PopupSignInViewController: UIViewController, GIDSignInUIDelegate  {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var emailEditText: UITextField!
    @IBOutlet weak var passwordEditText: UITextField!
    let modelView = SettingModelView()
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        emailEditText.delegate = self
        passwordEditText.delegate = self
    }
  
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if let error = error {
            let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                let alert = UIAlertController(title: "Error", message: "\(error)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }else {
                self.modelView.saveUser(email: authResult?.user.email ?? "", password: "")
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func signUpWithMailButtomTapped(_ sender: UIButton) {
        guard let email = emailEditText.text else {
            let alert = UIAlertController(title: "Error", message: "Email text field is empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        guard let password = passwordEditText.text else {
            let alert = UIAlertController(title: "Error", message: "Password text field is empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error {
                let alert = UIAlertController(title: "Error", message: "\(error.localizedDescription)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }else{
                self.modelView.saveUser(email: email, password: password)
                self.modelView.user = User(id: 1, password: password, email: email)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func signInWithMailButtonTapped(_ sender: UIButton) {
        guard let email = emailEditText.text else {
            let alert = UIAlertController(title: "Error", message: "Email text field is empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        if let _ = emailEditText.text?.firstIndex(of: "@")  {
            let alert = UIAlertController(title: "Error", message: "Invalid Email", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        guard let password = passwordEditText.text else {
            let alert = UIAlertController(title: "Error", message: "Password text field is empty", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] user, error in
            guard let _ = self else { return }
            self?.modelView.saveUser(email: email, password: password)
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func facebookBUttonTapped(_ sender: UIButton) {
    }
    @IBAction func googleButtonTapped(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
}
extension PopupSignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}








