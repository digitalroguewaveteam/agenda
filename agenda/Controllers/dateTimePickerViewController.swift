import UIKit
import FSCalendar
class DateTimePickerViewController: UIViewController {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timePicker: UIDatePicker!
    var selectedDate = Date()
    override func viewDidLoad() {
        super.viewDidLoad()
        timePicker.minimumDate = Date()
       dateLabel.text = Date().todoAtDateFormatter(date: selectedDate)
    }
    @IBAction func dateTimeChanged(_ sender: UIDatePicker) {
        dateLabel.text = Date().todoAtDateFormatter(date: sender.date)
        selectedDate = sender.date
    }
}


