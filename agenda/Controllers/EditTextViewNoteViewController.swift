//
//  EditTextViewNoteViewController.swift
//  agenda
//
//  Created by admin on 15/04/2019.
//  Copyright © 2019 Hamza Chaouachi. All rights reserved.
//

import UIKit

protocol EditNoteText {
    func textReceived(data: String)
}

class EditTextViewNoteViewController: UIViewController {
    var noteTitle: String?
    @IBOutlet weak var noteTextView: UITextView!
    var delegate: EditNoteText?
    var data : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        noteTextView.text = data
        noteTextView.becomeFirstResponder()
        navigationItem.title = noteTitle
    }
    @IBAction func DoneButtonTapped(_ sender: UIBarButtonItem) {
            delegate?.textReceived(data: noteTextView.text)
       navigationController?.popViewController(animated: false)
    }
    @IBAction func cancelButtonTapped(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: false)
    }
}
extension EditTextViewNoteViewController: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
      return  true
    }
}
