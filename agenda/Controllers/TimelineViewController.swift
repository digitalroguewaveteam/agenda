import UIKit
import FSCalendar

class TimelineViewController: UIViewController  {
    @IBOutlet weak var taskTable: UITableView!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendar: FSCalendar!
    var startPosition: CGPoint!
    var timeLineModelView : TimeLineModelView!
    var selectedTask : Note?
    @IBOutlet weak var selectedDayLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        taskTable.separatorStyle = .none
        guard let today = calendar.today else {return}
        timeLineModelView = TimeLineModelView(date: today)
        selectedDayLabel.text = Date().dateFormatter(date: today)
        calendar.dataSource = self
        calendar.delegate = self
        taskTable.delegate = self
        taskTable.dataSource = self
        taskTable.estimatedRowHeight = 30
        taskTable.rowHeight = UITableView.automaticDimension
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleSwipeCalendar(gesture:))))
        self.navigationController?.navigationBar.barTintColor = .white
    }
    @objc func handleSwipeCalendar(gesture: UIPanGestureRecognizer){
        if gesture.state == .began  {
            startPosition = gesture.location(in: self.view)
        }
        let endPosition = gesture.location(in: self.view)
        let difference = endPosition.y - startPosition.y
        if gesture.state == .ended {
            if difference < 0 {
                self.calendar.setScope(.week, animated: true)
            }else {
                self.calendar.setScope(.month, animated: true)
            }
        }
    }
    @objc func longTapTriggered(sender : UITapGestureRecognizer){
        if sender.state == UIGestureRecognizer.State.began {
            let tapLocation = sender.location(in: self.taskTable)
            let indexPath : NSIndexPath = self.taskTable.indexPathForRow(at: tapLocation)! as NSIndexPath
            let cell = self.taskTable.cellForRow(at: indexPath as IndexPath) as! TaskTableViewCell
            timeLineModelView.longTapHandler(cell:cell, indexPath: indexPath as IndexPath)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToHome"{
            if let task = selectedTask{
                let destinationVC =  segue.destination  as! HomeViewController
                destinationVC.homeModelView.reloadNotesWhenCategorySelected(category: timeLineModelView.getCategoryById(id: task.category))
                destinationVC.selectedCategory = timeLineModelView.getCategoryById(id: task.category)
                destinationVC.showTask = task
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if let today = calendar.today{
            timeLineModelView.loadTasksDate(date: calendar.selectedDate ?? today)
            taskTable.reloadData()
        }
    }
}
extension TimelineViewController: FSCalendarDataSource, FSCalendarDelegate {
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        UIView.animate(withDuration: 1) {
            self.calendarHeightConstraint = self.calendarHeightConstraint.setMultiplier(multiplier: bounds.height/self.view.frame.height)
            self.view.layoutIfNeeded()
        }
    }
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        timeLineModelView.loadTasksDate(date: date)
        selectedDayLabel.text = Date().dateFormatter(date: date)
        taskTable.reloadData()
    }
}
extension TimelineViewController: UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeLineModelView.taskTableViewNumberOfRowsInSection(tableView: taskTable)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  Bundle.main.loadNibNamed("TaskTableViewCell", owner: self, options: nil)?.first as! TaskTableViewCell
        timeLineModelView.assignTaskTableData(cell: cell, indexPath: indexPath)
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(longTapTriggered(sender:)))
        longTap.minimumPressDuration = 0.5
        cell.addGestureRecognizer(longTap)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToHome", sender: self)
    }
}


