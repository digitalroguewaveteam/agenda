import UIKit
import SideMenu
class HomeViewController: UIViewController, ShowPopup, PassSelectedCategoryProtocol {
    func receivedCategory(category: Category) {
        selectedCategory =  category
        categoryName.text =  category.name
        homeModelView.reloadNotesWhenCategorySelected(category: category)
        noteTableView.reloadData()
    }
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var noteTableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var nvBar: UINavigationItem!
    @IBOutlet var toolBar: UIView!
    var homeModelView = HomePageModelView()
    var selectedCategory : Category?
    var selectedNoteIndexPath : IndexPath?
    var showTask:Note?
    let searchBar = UISearchBar()
    var rightNavaigationBar : [UIBarButtonItem]!
    let coverView = UIView(frame: UIScreen.main.bounds)
    var titleView:UIView!
    var isSearching :Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSideMenu()
        searchBar.delegate = self
        noteTableView.delegate = self
        noteTableView.dataSource = self
        noteTableView.separatorStyle = .none
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = UIColor.gray
        rightNavaigationBar = [addButton,searchButton]
        noteTableView.rowHeight = UITableView.automaticDimension
        definesPresentationContext = true
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(showUpdateOrDeleteCategoryPopUp(_:)), name: Notification.Name("notificationShowUpdateOrDeleteCategoryPopUp"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goToAddNoteViewController), name: .addNoteNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayAddCategoryPopup), name: .showAddCategoryPopup , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNoteSettingsPopup), name: .showNoteSettingPopup, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showDateTimePicker), name: .showDateTimePicker, object: nil)
        if let category = selectedCategory{
            categoryName.text = category.name
        }else{
            categoryName.text = "ALL"
        }
    }
    @objc func showDateTimePicker(){
        showConvertNoteToTaskPopup(view: self, note: homeModelView.notesList[(selectedNoteIndexPath?.row)!])
    }
    
    func showNoteAtIndexPath(){
        noteTableView.reloadData()
    }
    func newNoteAdded(){
        homeModelView.reloadNotesWhenCategorySelected(category: selectedCategory)
        selectedNoteIndexPath = IndexPath(row: 0, section: 0)
        noteTableView.reloadData()
        noteTableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        viewWillAppear(true)
    }
    @objc func showNoteSettingsPopup(){
        guard let indexPath = selectedNoteIndexPath else {
            return
        }
        showStettingsNotePopup(view: self, note: homeModelView.notesList[indexPath.row])
    }
    
    @IBAction func toolBarButtonClicked(_ sender: UIButton) {
        guard let indexPath = selectedNoteIndexPath else {return}
        let cell =  noteTableView.cellForRow(at: indexPath) as? NoteTableViewCell
        switch sender.tag {
        case 0 : do {
            cell?.noteTextView.handleList()
            }
        case 1 : do {
            cell?.noteTextView.handleTitle(font: UIFont(name: "SFProText-Medium", size: 17)!)
            }
        case 2 : do {
            cell?.noteTextView.handleTitle(font: UIFont(name: "SFProText-Medium", size: 15)!)
            }
        case 3 : do {
            cell?.noteTextView.handleTitle(font: UIFont(name: "SFProText-Regular", size: 14)!)
            }
        default : do {
            view.endEditing(true)
            }
        }
    }
    @objc func goToAddNoteViewController(){
        if let category = self.selectedCategory {
            homeModelView.addNewEmptyNote(selectedCategory: category)
            newNoteAdded()
        }else {
            self.showChooseCategoryPopup(view: self)
        }
    }
    
    @IBAction func reloadAllNotesButtonTapped(_ sender: UIButton) {
        selectedCategory = nil
        categoryName.text = "ALL"
        homeModelView.loadData()
        noteTableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        if let selectedCategory = self.selectedCategory {
            //            categoriesCollectionView.scrollToItem(at: IndexPath(row: homeModelView.getCategoryIndex(id: selectedCategory.id!), section: 0), at: .centeredHorizontally, animated: true)
            //            categoryName.text = selectedCategory.name
            //            if let idTask = showTask?.id {
            //                if let row = homeModelView.getNoteIndexById(id: idTask){
            //                    selectedNoteIndexPath = IndexPath(row: row , section: 0)
            //                    showTask = nil
            //                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)){
            //                        self.noteTableView.selectRow(at: IndexPath(row: row , section: 0), animated: true, scrollPosition: .top)
            //                    }
            //                }
            //            }
        }
    }
    @objc func displayAddCategoryPopup(){
        //self.showAddCategoryPopup(animated: true, view: self, collectionView: categoriesCollectionView)
    }
    @IBAction func addButtonTapped(_ sender: UIBarButtonItem) {
        view.endEditing(true)
        if let category = selectedCategory {
            homeModelView.addNewEmptyNote(selectedCategory: category)
            selectedNoteIndexPath = nil
            noteTableView.reloadData()
        }else {
            self.showChooseAddCategorieOrAddNotePopup(view: self)
        }
    }
    @IBAction func searchButtonTapped(_ sender: UIBarButtonItem) {
        searchLayout()
    }
    override func viewWillDisappear(_ animated: Bool) {
        noteTableView.reloadData()
        //categoriesCollectionView.reloadData()
    }
    @objc func showUpdateOrDeleteCategoryPopUp(_ notification: NSNotification){
        if let dict = notification.userInfo as NSDictionary? {
            if let id = dict["categoryId"] as? Int{
                //showUpdateOrDeleteCategoryPopup(animated:true, view:self, collectionView:categoriesCollectionView ,idCategory:id)
            }
        }
    }
    @IBAction func addCategoryButtonTapped(_ sender: UIButton) {
        //showAddCategoryPopup(view: self, collectionView: categoriesCollectionView)
    }
    @IBAction func menuButtomTapped(_ sender: UIBarButtonItem) {
        let menu = storyboard!.instantiateViewController(withIdentifier: "RightMenu")  as! SideMenuNavigationController
        let slideViewController = menu.topViewController as? SlideViewController
        slideViewController?.passCategoryProtocolDelegate = self
        if let category =  selectedCategory {
            slideViewController?.selectedCategory = category
        }
        present(menu, animated: true, completion: nil)
    }
    func searchLayout(){
        navigationController?.navigationBar.tintColor = .blue
        categoryName.isHidden = true
        searchBar.isHidden = false
        navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelSearch))]
        titleView = navigationItem.titleView
        navigationItem.hidesBackButton = true
        navigationItem.titleView = searchBar
        searchBar.sizeToFit()
        searchBar.becomeFirstResponder()
    }
    @objc  func cancelSearch(){
        cancelSearchLayout()
    }
    func cancelSearchLayout(){
        categoryName.isHidden = false
        navigationController?.navigationBar.tintColor = .gray
        isSearching = false
        searchBar.isHidden = true
        navigationItem.hidesBackButton = false
        navigationItem.titleView = titleView
        navigationItem.rightBarButtonItems = rightNavaigationBar
        coverView.removeFromSuperview()
        searchBar.endEditing(true)
        noteTableView.reloadData()
    }
    func selectedNoteLayout(cell:NoteTableViewCell, indexPath:IndexPath){
        cell.lastModificationLabel.isHidden = false
        //        cell.contentView.layer.borderColor =  UIColor().colorFromHex(homeModelView.findCategoryById(id: homeModelView.notesList[indexPath.row].category)?.color ?? "ffffff").cgColor
        //        cell.contentView.layer.borderWidth = 0.5
        //        cell.contentView.backgroundColor = UIColor().colorFromHex(homeModelView.findCategoryById(id: homeModelView.notesList[indexPath.row].category)?.color ?? "ffffff").withAlphaComponent(0.07)
        //        cell.contentView.backgroundColor = UIColor().colorFromHex(homeModelView.findCategoryById(id: homeModelView.notesList[indexPath.row].category)?.color ?? "ffffff").withAlphaComponent(0.07)
        if cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font: cell.noteTextView.font!) > cell.noteTextViewHeightContstraint.constant - cell.noteTextView.font!.lineHeight {
            cell.noteTextView.sizeThatFits(cell.frame.size)
            cell.noteTextViewHeightContstraint.constant = cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font:cell.noteTextView.font!) +  cell.noteTextView.font!.lineHeight
            cell.layoutIfNeeded()
        }
        cell.calendarIcon.isHidden = false
        cell.settingsIcon.isHidden = false
        cell.updateConstraints()
        cell.layoutIfNeeded()
        cell.noteTextView.becomeFirstResponder()
        cell.noteTextView.isEditable = true
        cell.noteTextView.isSelectable = true
        cell.noteTextView.isUserInteractionEnabled = true
        cell.noteTextView.resolveTags()
        cell.titreNote.isEnabled = true
    }
    private func setupSideMenu() {
        // Define the menus
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "RightMenu") as? SideMenuNavigationController
        
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
    }
}
extension HomeViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return homeModelView.fiteredNotes?.count ?? 0
        }else{
            return  homeModelView.noteTableViewNumberOfRowsInSection(tableView: tableView)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        noteTableView.beginUpdates()
        let cell =  Bundle.main.loadNibNamed("NoteTableViewCell", owner: self, options: nil)?.first as! NoteTableViewCell
        if isSearching {
            homeModelView.assignDataWhileSearching(cell: cell, indexPath: indexPath)
        }else{
            homeModelView.asignDataToNoteTableView(cell: cell, indexPath: indexPath)
            cell.noteTextView.delegate = self
            cell.titreNote.delegate = self
        }
        if cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font: cell.noteTextView.font!) < cell.noteTextViewHeightContstraint.constant - cell.noteTextView.font!.lineHeight {
            cell.noteTextView.sizeThatFits(cell.frame.size)
            cell.noteTextViewHeightContstraint.constant = cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font:cell.noteTextView.font!) +
                2*cell.noteTextView.font!.lineHeight
            cell.layoutIfNeeded()
        }
        cell.noteTextView.resolveTags()
        
        noteTableView.endUpdates()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let  cell = tableView.cellForRow(at: indexPath) as? NoteTableViewCell {
            selectedNoteIndexPath = indexPath
            if isSearching{
                guard let list = homeModelView.fiteredNotes else {return}
                selectedCategory = homeModelView.getCategoryById(id: list[indexPath.row].category)
                homeModelView.reloadNotesWhenCategorySelected(category: selectedCategory)
                isSearching = false
                cancelSearchLayout()
                noteTableView.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250)){
                    //                    if let indexRow = self.homeModelView.getNoteIndexById(id: list[indexPath.row].id ?? -1) {
                    //                        self.noteTableView.scrollToRow(at: IndexPath(row: indexRow, section: 0), at: .top, animated: false)
                    //                        if let cellule = tableView.cellForRow(at: IndexPath(row: indexRow, section: 0)) as? NoteTableViewCell {
                    //                            self.selectedNoteLayout(cell: cellule, indexPath: IndexPath(row: indexRow, section: 0) )
                    //                            self.noteTableView.beginUpdates()
                    //                            self.noteTableView.endUpdates()
                    //                        }
                    //                    }
                }
            }else{
                self.noteTableView.beginUpdates()
                self.selectedNoteLayout(cell: cell, indexPath: indexPath )
                self.noteTableView.endUpdates()
            }
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? NoteTableViewCell {
            cell.isSelected = false
            cell.calendarIcon.isHidden = true
            cell.settingsIcon.isHidden = true
            cell.contentView.backgroundColor = UIColor.white
            cell.contentView.layer.borderColor =  UIColor.white.cgColor
            cell.noteTextView.isSelectable = false
            cell.noteTextView.isEditable = false
            cell.noteTextView.isUserInteractionEnabled = false
            cell.titreNote.isEnabled = false
            cell.titreNote.borderStyle = .none
            if cell.noteTextView.contentSize.height > 216 {
                cell.noteTextViewHeightContstraint.constant = 216
                cell.layoutIfNeeded()
            }
            cell.lastModificationLabel.isHidden = true
            noteTableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension HomeViewController : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        let cursorPosition  = textView.offset(from: textView.beginningOfDocument, to: textView.selectedTextRange!.start)
        textView.resolveTags()
        guard let index = selectedNoteIndexPath else {
            return
        }
        if let cell = noteTableView.cellForRow(at: index) as? NoteTableViewCell {
            if cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font: textView.font!) != cell.noteTextViewHeightContstraint.constant - 2*textView.font!.lineHeight {
                cell.noteTextViewHeightContstraint.constant = cell.noteTextView.text.heightWithConstrainedWidth(width: cell.frame.width, font: textView.font!) + 5*textView.font!.lineHeight
                cell.layoutIfNeeded()
                UIView.performWithoutAnimation {
                    noteTableView.beginUpdates()
                    noteTableView.endUpdates()
                }
            }
        }
        if let newPosition = textView.position(from: textView.beginningOfDocument, offset: cursorPosition) {
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        noteTableView.allowsSelection = false
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.inputAccessoryView = toolBar
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        guard  let indexPath = selectedNoteIndexPath else {
            return
        }
        noteTableView.allowsSelection = true
        homeModelView.updateNote(index: indexPath.row, noteBody: textView.textStorage , title: nil)
        if let selectedCategory = selectedCategory {
            homeModelView.reloadNotesWhenCategorySelected(category: selectedCategory)
        }else {
            homeModelView.loadNotes()
        }
    }
}
extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.inputAccessoryView = toolBar
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let index = selectedNoteIndexPath?.row {
            textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            homeModelView.updateNote(index: index, noteBody: nil, title: textField.attributedText)
        }
    }
}
extension HomeViewController:  UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        isSearching = true
        coverView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        self.view.addSubview(coverView)
        return true
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        var text  = searchText
        
        while text.hasPrefix(" ") {
            text.removeFirst()
        }
        if !text.isEmpty {
            
            homeModelView.searchFullText(text: text)
            if homeModelView.fiteredNotes?.count != 0 && homeModelView.fiteredNotes != nil  {
                coverView.removeFromSuperview()
                noteTableView.reloadData()
            }else{
                coverView.frame = UIScreen.main.bounds
                self.view.addSubview(coverView)
            }
        }else {
            coverView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
            coverView.frame = UIScreen.main.bounds
            self.view.addSubview(coverView)
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if isSearching {
            //            let origin = categoriesCollectionView.frame.origin
            //            coverView.frame = CGRect(x: 0 , y: origin.y, width: self.view.frame.width, height: categoriesCollectionView.frame.height)
            //            view.addSubview(coverView)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
