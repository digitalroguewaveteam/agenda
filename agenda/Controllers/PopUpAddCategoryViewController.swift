import UIKit
import PopupDialog

class PopUpAddCategoryViewController: UIViewController {
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var categoryColorButton1: UIButton!
    @IBOutlet weak var categoryColorButton2: UIButton!
    @IBOutlet weak var categoryColorButton3: UIButton!
    @IBOutlet weak var categoryColorButton4: UIButton!
    var categoryName: String = "Untitled"
    var categoryColor : String?
    var selectedButton: UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryNameTextField.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        categoryNameTextField.text = categoryName 
        if let color =  categoryColor {
            switch color {
            case UIColor.myBlue : do {
                selectedButton = categoryColorButton1
                categoryColorButton1.setImage(UIImage(named: "bookmark-outline-blue"), for: .normal)
                }
            case UIColor.myGreen : do {
                selectedButton = categoryColorButton2
                categoryColorButton2.setImage(UIImage(named: "bookmark-outline-green"), for: .normal)
                }
            case UIColor.myPink : do {
                selectedButton = categoryColorButton3
                categoryColorButton3.setImage(UIImage(named: "bookmark-outline-pink"), for: .normal)
                }
            default:
                selectedButton = categoryColorButton4
                categoryColorButton4.setImage(UIImage(named: "bookmark-outline-orange"), for: .normal)
            }
        }
    }
    init(text: String?, color: String?) {
        super.init(nibName: "AddCategoryPopUp", bundle: nil)
        if let txt = text {
            categoryName = txt
        }
        if let couleur = color {
            categoryColor = couleur
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func endEditing() {
        view.endEditing(true)
    }
    @IBAction func endEdidtingCategoryNameTextField(_ sender: UITextField) {
        categoryName = sender.text!
    }
    @IBAction func colorButtonTapped(_ sender: UIButton) {
        if let lastSelectedItem = selectedButton {
            switch lastSelectedItem {
            case categoryColorButton1: lastSelectedItem.setImage(UIImage(named: "bookmark-blue"), for: .normal)
            case categoryColorButton2: lastSelectedItem.setImage(UIImage(named: "bookmark-green"), for: .normal)
            case categoryColorButton3: lastSelectedItem.setImage(UIImage(named: "bookmark-pink"), for: .normal)
            default:lastSelectedItem.setImage(UIImage(named: "bookmark-orange"), for: .normal)
            }
        }
        switch sender.tag {
        case 1: do {
            categoryColor = UIColor.myBlue
            sender.setImage(UIImage(named: "bookmark-outline-blue"), for: .normal)
            }
        case 2: do {
            categoryColor = UIColor.myGreen
            sender.setImage(UIImage(named: "bookmark-outline-green"), for: .normal)
            }
        case 3: do {
            categoryColor = UIColor.myPink
            sender.setImage(UIImage(named: "bookmark-outline-pink"), for: .normal)
            }
        default: do {
            categoryColor = UIColor.myOrange
            sender.setImage(UIImage(named: "bookmark-outline-orange"), for: .normal)
            }
        }
        selectedButton = sender
    }
    @IBAction func categoryTextFieldValueChanged(_ sender: UITextField) {
        categoryName = sender.text!
    }
}
extension PopUpAddCategoryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        categoryName = textField.text!
        return true
    }
}
