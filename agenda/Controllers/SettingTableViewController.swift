import UIKit
import FirebaseAuth
import GoogleSignIn
class SettingTableViewController: UITableViewController , ShowPopup{
    @IBOutlet weak var signInCell: UITableViewCell!
    var modelView = SettingModelView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  modelView.user != nil {
                        logoutLayout()
        }else{
            signInCell.textLabel?.text = "signIn/Signup"
            
        }
    }
   func  logoutLayout(){
        signInCell.accessoryType = .none
        signInCell.detailTextLabel?.text = "Logout"
        signInCell.textLabel?.text = modelView.user?.email
    }
    override func viewWillAppear(_ animated: Bool) {
        if  modelView.user != nil {
            logoutLayout()
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if let _ = modelView.user {
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                    DB.instance.deleteUser()
                    modelView.user = nil
                    self.viewDidLoad()
                    signInCell.textLabel?.text = "signIn/Signup"
                    signInCell.detailTextLabel?.text = ""
                    self.logoutLayout()
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)
                }
            }else{
                showSignInPopUp(view: self)
            }
        }
    }
}
extension SettingTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
}
